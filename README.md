# Assigment for tests of tommy web page

This is a project containing 2 features files to cover the following scenarios:

1.  Verify that user can see specific errors when not enough information is provided during registration process
2.  Verify that user can create an account with valid data
3.  Verify that registered user can add a new address.

## Features

-   TypeScript
-   Expect-webdriverio
-   Page Object Pattern
-   Gherkin / Cucumber
-   ESlint
-   Prettier
-   Allure report (screenshots on failure)

## Requirements

node >= 14.15.x - [how to install Node](https://nodejs.org/en/download/)

JDK >= install the JDK and set the JAVA_HOME variable in your environment.

## Getting Started

Clone or download the project.

Open a terminal or Bash CLI - [how to install git bash windows](https://git-scm.com/downloads)

Then, in the terminal, go into the folder where you have cloned or extracted the project. Or run the commands in the terminal of your IDE.

Install the dependencies:

```bash
npm install
```
On my, clean test machine i needed to install the chromedriver aswell

Install the chrome driver:
```bash
npm install chromedriver
```

Run all features:

```bash
npm run test:allfeatures
```

Run login tests:

```bash
npm run test:login
```

Run accounts tests:

```bash
npm run test:account
```

## Reports

### Allure

Run this command to generate the allure report

```bash
npm run report:generate
```

To run the Allure report, from the bash CLI, please make sure you have installed JDK and set the JAVA_HOME variable in your environment.
