Feature: Change account details
    Log into the tommy page and change the address acount for a known account.

    # Scenario: Check the empty addressbook
    #     Given Im logged in and i have opened my acount details
    #     When I open the addresbook
    #     Then There is no address displayed

    Scenario: Add a new address to the addressbook
        Given Im logged in and i have opened my acount details
        When I open the addresbook
        And I want to add an address
        And I add a firstname "Jorrit"
        And I add a lastname "Krijnen"
        And I add a streetname "Louise Marie Loeberplnts"
        And i add a housnumber "93"
        And i add a city "Amsterdam"
        And i add a postalcode "1062 DD"
        And i click on save
        Then My streetname will displayed in my account "Louise Marie Loeberplnts"