Feature: Performing a login on tommy page
    Check the login functionality of the .nl.tommy.com page.

    Scenario: Login with a unknown user
        Given I'm on the main tommy page
        When I open the login section
        Then I log in with a unknown user
        And a login error message will be shown

    Scenario: Register without correctpassword
        Given I'm on the main tommy page
        When I open the login section
        And I add credentials for a new customer but with wrong second password
        Then password doesnt match error will be shown

    Scenario: Login with a known user
        Given I'm on the main tommy page
        When I open the login section
        And I log in with a user "jorrit.krijnen@iovio.com"
        And I put in password "Iovio123!@#"
        Then Im logged into the tommy page