export const waitAndClick = (el: { waitForDisplayed: (arg0: { timeout: any; }) => void; click: () => void; }, timeout: any) => {
    el.waitForDisplayed({timeout})
    el.click();
}

export const loginExsistAndClick = (elInput: { isExisting: () => any; }, elButton: { click: () => void; }) => {
    if (elInput.isExisting()){
        elButton.click()
    }
    else{
        browser.pause(1000)
        elButton.click()
    }
}