class AddressPage {
    get inputFirstname() {
        return $("//input[@id='firstName']");
    }

    get inputLastname() {
        return $("//input[@id='lastName']");
    }

    get inputStreet() {
        return $("//input[@id='address1']");
    }

    get inputHouseNumber() {
        return $("//input[@id='address2']");
    }

    get inputCity() {
        return $("//input[@id='city']");
    }

    get inputPostalcode() {
        return $("//input[@id='zipCode']");
    }

    get buttonSaveAddAddress() {
        return $(
            "//button[@class='Button Button__secondary address-form__button-save']",
        );
    }

    get buttonCancelAddAddress() {
        return $("//button[@class='Button Button__secondary--inverse ']");
    }

    checkAddressPageIsDisplayed() {
        const addressWindowOpened = this.buttonSaveAddAddress.isClickable();

        if (addressWindowOpened) {
            console.log('address window is opened, continue');
        } else {
            this.buttonSaveAddAddress.waitForDisplayed({timeout:2000})
        }
    }
}

export const addressPage = new AddressPage();
