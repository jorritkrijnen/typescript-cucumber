import { loginExsistAndClick } from '../helper/helper';
import { mainPage } from './Main.page';
class LoginPage {
    get emailExistCust() {
        return $("//input[@name='logonId']");
    }

    get correctInput() {
        return $("//input[@class='input__email input__input--success']");
    }

    get passwordExistCust() {
        return $("//input[@name='logonPassword']");
    }

    get buttonAanmelding() {
        return $("//button[@class='login__submit button']");
    }

    get errorText() {
        return $(
            "//p[contains(text(),'Je gebruikersnaam en wachtwoord matchen niet. Prob')]",
        );
    }

    get emailNewCust() {
        return $("//input[@name='email1']");
    }

    get passwordNewCust() {
        return $(
            "//div[@class='form-field input input--accessible-label register__password']//following-sibling::input",
        );
    }

    get passwordNewCustVerify() {
        return $("//input[@name='logonPasswordVerify']");
    }

    get buttonRegister() {
        return $("//button[@class='button--full-width button']");
    }

    get checkboxAlgemeneVoorwaarden() {
        return $("//input[@name='signUpForTermsCondition1']");
    }

    get errorPasswordNotMatch() {
        return $(
            "//div[contains(text(),'Oeps! De wachtwoorden matchen niet')]",
        );
    }

    checkLoginPageisOpened() {
        const loginIsOpened = this.buttonAanmelding.isClickable();

        if (loginIsOpened) {
            console.log('login section is opened, continue');
        } else {
            this.buttonAanmelding.waitForDisplayed();
            console.log('+++ button aanmelden bestaat +++');
        }
    }

    checkLoginText() {
        if (loginPage.errorText.isExisting()) {
            expect(this.errorText).toHaveText(
                'Je gebruikersnaam en wachtwoord matchen niet. Probeer het nog eens.',
            );
        } else {
            browser.pause(1000);
            expect(this.errorText).toHaveText(
                'Je gebruikersnaam en wachtwoord matchen niet. Probeer het nog eens.',
            );
        }
    }

    openLoginscreenAndLogin() {
        mainPage.openLoginSection();
        this.checkLoginPageisOpened();
        this.emailExistCust.addValue('jorrit.krijnen@iovio.com');
        this.passwordExistCust.addValue('Iovio123!@#');

        loginExsistAndClick(
            loginPage.emailExistCust,
            loginPage.buttonAanmelding,
        );
    }
}

export const loginPage = new LoginPage();
