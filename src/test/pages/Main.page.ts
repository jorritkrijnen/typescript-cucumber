import { waitAndClick } from "../helper/helper";
import { loginPage } from "./Login.page";

class MainPage {

  get buttonLogin() {
    return $("//button[@class='header__link']");
  }

  get buttonCookie() {
    return $("//button[@class='cookie-notice__agree-button button']");
  }

  get buttonAccount() {
    return $("//a[@class='header__action account-link']")
  }

  get buttonAfmelden() {
    return $("//button[@data-qa='sign-out-button']");
  }

  open() {
    browser.url('https://nl.tommy.com/');
    
    const cookiePresent = this.buttonCookie.isClickable();

    if (cookiePresent) {
      waitAndClick(this.buttonCookie, 2000);
    }
  }

  openLoginSection() {
    browser.pause(1000);
    
    waitAndClick(this.buttonLogin, 2000);
    
    browser.pause(1000);
    expect(loginPage.emailExistCust).toBeDisplayed();
  }
}

export const mainPage = new MainPage();
