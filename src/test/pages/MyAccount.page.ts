class MyAccountPage {
    get buttonAddressBook() {
        return $("//h3[contains(text(),'Adresboek')]");
    }

    get buttonAddAddress() {
        return $("//button[@class='Button Button__secondary ']");
    }

    get addressBookEmpty() {
        return $("//p[contains(text(),'Er zijn geen adressen opgeslagen')]");
    }

    get adressBookHasValue() {
        return $("//option[@value='0']")
    }
}

export const myAccountPage = new MyAccountPage();
