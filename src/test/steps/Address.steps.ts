import { When } from 'cucumber';
import { addressPage } from '../pages/Address.Page';

When(/^I add a firstname "([^"]*)?"$/, (firstname) => {
    addressPage.inputFirstname.addValue(firstname);
});

When(/^I add a lastname "([^"]*)?"$/, (lastname) => {
    addressPage.inputLastname.addValue(lastname);
});

When(/^I add a streetname "([^"]*)?"$/, (streetname) => {
    addressPage.inputStreet.addValue(streetname);
});

When(/^i add a housnumber "([^"]*)?"$/, (housenumber) => {
    addressPage.inputHouseNumber.addValue(housenumber);
});

When(/^i add a city "([^"]*)?"$/, (city) => {
    addressPage.inputCity.addValue(city);
});

When(/^i add a postalcode "([^"]*)?"$/, (postalcode) => {
    addressPage.inputPostalcode.addValue(postalcode);
});

When('i click on save', () => {
    addressPage.buttonSaveAddAddress.click();
});