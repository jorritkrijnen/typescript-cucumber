import { When, Then } from 'cucumber';
import { loginPage } from '../pages/Login.page';
import { loginExsistAndClick } from '../helper/helper';
import { mainPage } from '../pages/Main.page';

When(/^I log in with a unknown user$/, () => {
    loginPage.checkLoginPageisOpened();
    loginPage.emailExistCust.addValue('test@test.com');
    loginPage.passwordExistCust.addValue('appeltaart1');

    loginExsistAndClick(loginPage.emailExistCust, loginPage.buttonAanmelding);
});

Then('a login error message will be shown', () => {
    loginPage.checkLoginText();
});

When('I add credentials for a new customer but with wrong second password', () => {
        loginPage.checkLoginPageisOpened();

        loginPage.emailNewCust.addValue('test@test.com');
        loginPage.passwordNewCust.addValue('appeltaart1');
        loginPage.passwordNewCustVerify.addValue('Appeltaart2');

        loginPage.checkboxAlgemeneVoorwaarden.click;

        loginExsistAndClick(loginPage.emailNewCust, loginPage.buttonRegister);
    },
);

Then('password doesnt match error will be shown', () => {
    expect(loginPage.errorPasswordNotMatch).toHaveText(
        'Oeps! De wachtwoorden matchen niet',
    );
});

When(/^I log in with a user "([^"]*)?"$/, (username) => {
    loginPage.emailExistCust.addValue(username);
});

When(/^I put in password "([^"]*)?"$/, (password) => {
    loginPage.passwordExistCust.addValue(password);
});

Then('Im logged into the tommy page', () => {
    loginExsistAndClick(loginPage.emailExistCust, loginPage.buttonAanmelding);

    expect(mainPage.buttonAfmelden).toExist;

    mainPage.buttonAfmelden.click();
});
