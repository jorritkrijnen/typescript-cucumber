import { Given, When } from 'cucumber';
import { mainPage } from '../pages/Main.page';

Given(/^I'm on the main tommy page$/, () => {
    mainPage.open();
});

When(/^I open the login section$/, () => {
    mainPage.openLoginSection();
});

When(/^open login screen$/, () => {
    mainPage.buttonLogin.click();
})
