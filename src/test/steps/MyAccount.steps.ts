import { Given, Then, When } from 'cucumber';
import { waitAndClick } from '../helper/helper';
import { addressPage } from '../pages/Address.Page';
import { loginPage } from '../pages/Login.page';
import { mainPage } from '../pages/Main.page';
import { myAccountPage } from '../pages/MyAccount.page';

Given('Im logged in and i have opened my acount details', () => {
    mainPage.open();
    loginPage.openLoginscreenAndLogin();
    mainPage.buttonAccount.click();
});

When('I open the addresbook', () => {
    waitAndClick(myAccountPage.buttonAddressBook, 3000)
});

When('There is no address displayed', () => {
    expect(myAccountPage.addressBookEmpty).toHaveText('Er zijn geen adressen opgeslagen');

    mainPage.buttonAfmelden.click();
});

When('I want to add an address', () => {
    waitAndClick(myAccountPage.buttonAddAddress, 3000)

    addressPage.checkAddressPageIsDisplayed();
});

Then(/^My streetname will displayed in my account "([^"]*)?"$/, (streetname) => {
    myAccountPage.adressBookHasValue.waitForDisplayed({timeout:3000})

    expect(myAccountPage.adressBookHasValue).toHaveText(streetname);

    mainPage.buttonAfmelden.click();
});



